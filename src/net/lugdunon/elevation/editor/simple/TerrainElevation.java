package net.lugdunon.elevation.editor.simple;

import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

import net.lugdunon.io.EnhancedDataInputStream;
import net.lugdunon.state.Account;
import net.lugdunon.state.State;
import net.lugdunon.util.FastMath;
import net.lugdunon.world.terrain.Chunk;
import net.lugdunon.world.terrain.Terrain;

import org.json.JSONException;

public class TerrainElevation
{
	public static Set<Integer> updateElevation(Account a,EnhancedDataInputStream data) throws IOException, JSONException
    {
		byte e=data.readByte ();
		int  x=data.readShort()+(a.getEditingLocation().getX()-(a.getScreenW()/2));
		int  y=data.readShort()+(a.getEditingLocation().getY()-(a.getScreenH()/2));
		int  w=data.readByte ();
		int  h=data.readByte ();
		
		return(updateElevation(a, x, y, w, h, e));
    }
	
	public static Set<Integer> updateElevation(Account a, int x, int y, byte elevation) throws IOException, JSONException
    {
		return(updateElevation(a, x, y, 1, 1, elevation));
    }
	
	public static Set<Integer> updateElevation(Account a, int x, int y, int w, int h, byte elevation) throws IOException, JSONException
    {
		int           index;
		Chunk         c;
		Terrain       terrain      =State.instance().getWorld().getInstance(a.getActiveCharacter().getInstanceId()).getTerrain();
		Set<Integer>  chunksUpdated=new TreeSet<Integer>();

		//SET
		for(int j=y;j<y+h;j++)
		{
			for(int i=x;i<x+w;i++)
			{
				index=FastMath.wrap(i,terrain.getSize())*terrain.getSize()+FastMath.wrap(j,terrain.getSize());

				if(terrain.getElevationData()[index] == elevation-1 || terrain.getElevationData()[index] == elevation+1)
				{
					terrain.setElevationData(index,elevation);
				}
			}
		}

		//COLLATE UPDATED CHUNKS
		for(int j=y-1;j<y+h+3;j++)
		{
			for(int i=x-1;i<x+w+1;i++)
			{
				c=terrain.getChunkForCoord(i,j);
				c.recomputeImpassabilityFlags();
				chunksUpdated.add(c.getId());
			}
		}
		
	    return(chunksUpdated);
    }
}