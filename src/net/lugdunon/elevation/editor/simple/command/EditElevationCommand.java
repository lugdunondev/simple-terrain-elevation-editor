package net.lugdunon.elevation.editor.simple.command;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import net.lugdunon.command.CommandProperties;
import net.lugdunon.command.CommandRequest;
import net.lugdunon.command.core.IServerInvokedCommand;
import net.lugdunon.command.core.gm.GmOnlyCommand;
import net.lugdunon.command.ex.ConsoleFiredCommandNotSupportedException;
import net.lugdunon.elevation.editor.simple.TerrainElevation;
import net.lugdunon.state.State;
import net.lugdunon.state.character.PlayerCharacter;
import net.lugdunon.world.terrain.Terrain;

public class EditElevationCommand extends GmOnlyCommand implements IServerInvokedCommand
{
	protected Response iRes;
	
	////
	
	@Override
	public String getCommandId()
	{
	    return("CORE.COMMAND.EDIT.ELEVATION.SIMPLE");
	}

	@Override
	public String getName()
	{
	    return("Edit Elevation (Simple)");
	}
	
	@Override
	public String getDescription()
	{
	    return("Make modifications terrain elevation (Editor).");
	}
	
	@Override
	public int getCommandLength()
	{
	    return(iRes.length());
	}

	@Override
    public void handle(CommandProperties props) throws IOException, ConsoleFiredCommandNotSupportedException
    {
		@SuppressWarnings("unchecked")
        Set<Integer> chunksUpdated=(Set<Integer>) props.get("chunksUpdated");
		
		if(chunksUpdated == null || chunksUpdated.size() == 0){return;}

		long                  iid  =props.getLong("instanceId");
		List<PlayerCharacter> chars=State.instance().listActiveCharactersInInstance(iid);

		Response oRes;
		int      pushedChunkCount;
		
		for(PlayerCharacter c:chars)
		{
			pushedChunkCount=0;
			iRes            =initializeInternalResponse();
			
			for(int i:chunksUpdated)
			{
				if(c.isChunkVisibleOnMinimap(i))
				{
					pushedChunkCount++;
					State.instance().getWorld().getInstance(iid).getTerrain().getTerrainChunk(i, iRes.out);
				}
			}
			
			iRes.out.write(Terrain.CHUNK_UPDATE_MODE_NO_PI);
			
			oRes=initializeResponse();
			oRes.out.writeShort(0);
			oRes.out.writeShort(pushedChunkCount);
			oRes.out.write(iRes.bytes());
			
			c.getAccount().getConnection().sendMessage(oRes.bytes(),0,oRes.length());
		}
    }

	@Override
    public void handleAsGm(CommandRequest request) throws IOException
    {
		try
		{
			CommandProperties props=new CommandProperties();
			
			props.set("chunksUpdated",TerrainElevation.updateElevation(request.getOrigin().getAccount(),request.getData()));
			props.set("instanceId",   request.getOrigin().getAccount().getActiveCharacter().getInstanceId());

			handle(props);
		}
		catch(Exception e)
		{
			;
		}
    }
	
	public boolean hasClientSide()
	{
		return(true);
	}
}
