Namespace.declare("net.lugdunon.elevation.editor.simple.command");
Namespace.newClass("net.lugdunon.elevation.editor.simple.command.EditElevationCommand","net.lugdunon.command.core.Command");

// //

net.lugdunon.elevation.editor.simple.command.EditElevationCommand.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.elevation.editor.simple.command.EditElevationCommand,"init",[initData]);
	
	this.editE;
	this.editX;
	this.editY;
	this.editW;
	this.editH;
	
	return(this);
};

net.lugdunon.elevation.editor.simple.command.EditElevationCommand.prototype.opInit=function(initData)
{
	this.editE=initData.e;
	this.editX=initData.x;
	this.editY=initData.y;
	this.editW=initData.w;
	this.editH=initData.h;
};

net.lugdunon.elevation.editor.simple.command.EditElevationCommand.prototype.getCommandLength=function()
{
    return(7);
};

net.lugdunon.elevation.editor.simple.command.EditElevationCommand.prototype.buildCommand=function(dataView)
{
	dataView.writeInt8  (this.editE);
	dataView.writeUint16(this.editX);
	dataView.writeUint16(this.editY);
	dataView.writeUint8 (this.editW);
	dataView.writeUint8 (this.editH);
};

net.lugdunon.elevation.editor.simple.command.EditElevationCommand.prototype.commandResponse=function(res)
{
	game.chunkManager.chunksUpdated(res,true);
};