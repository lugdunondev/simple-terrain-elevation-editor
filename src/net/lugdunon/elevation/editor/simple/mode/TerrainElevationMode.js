Namespace.declare("net.lugdunon.elevation.editor.simple.mode");
Namespace.require("net.lugdunon.ui.Label");
Namespace.require("net.lugdunon.states.tiledGame.modes.terrain.SimpleButton");
Namespace.require("net.lugdunon.states.tiledGame.modes.terrain.BrushSizeWidget");
Namespace.newClass("net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode","net.lugdunon.states.modes.BaseGameMode");

////

net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.MAX_ELEVATION= 127;
net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.MIN_ELEVATION=-127;

net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.brushSizeWidget;
net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.elevationUpButton;
net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.elevationDownButton;
net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.elevationToggleButton;

net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode,"init",[initData]);

	this.bmWidth      =game.tileSetDef.maxDrawFieldSize*32;
	this.curElevLabel =new net.lugdunon.ui.Label().init();
	this.elevation    =0;
	this.resetSend    =false;
	this.teod         =false;
	this.tilesetImg   =assetManager.getImage("TILESET");
	this.cursorLoc    ={x:-1,y:-1};
	
	this.boundingRect ={m:false,x:0,y:0,w:0,h:0};
	
	// INIT BUTTON IMAGE
	this.image       =game.makeCanvas(64,64);
	this.imageContext=this.image.getContext("2d");
	
	this.imageContext.drawImage(
		assetManager.getImage("ICONS"),
		game.ui.icons.ACTION_ELEVATION.offset.x,
		game.ui.icons.ACTION_ELEVATION.offset.y,
		game.ui.icons.ACTION_ELEVATION.size  .w,
		game.ui.icons.ACTION_ELEVATION.size  .h,

		32-game.ui.icons.ACTION_ELEVATION.size.w/2,
		32-game.ui.icons.ACTION_ELEVATION.size.h/2,
		
		game.ui.icons.ACTION_ELEVATION.size  .w,
		game.ui.icons.ACTION_ELEVATION.size  .h
	);
	
	// INIT BRUSH SIZE IMAGE
	if(net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.elevationUpButton == null)
	{
		net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.elevationUpButton=new net.lugdunon.states.tiledGame.modes.terrain.SimpleButton().init(
			{
				id           :"temup",
				delegate     :this,
				icon         :net.lugdunon.states.tiledGame.modes.terrain.SimpleButton.ICONS.UP,
				doNotRegister:true,
				position     :{x:game.getCanvasWidthInPixels()-64, y:0}
			}
		);
		net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.elevationDownButton=new net.lugdunon.states.tiledGame.modes.terrain.SimpleButton().init(
			{
				id           :"temdown",
				delegate     :this,
				icon         :net.lugdunon.states.tiledGame.modes.terrain.SimpleButton.ICONS.DOWN,
				doNotRegister:true,
				position     :{x:game.getCanvasWidthInPixels()-64, y:50}
			}
		);
		net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.elevationToggleButton=new net.lugdunon.states.tiledGame.modes.terrain.SimpleButton().init(
			{
				id           :"eltoggle",
				delegate     :this,
				icon         :
					game.uiSettings.getFlagValue(game.uiSettings.flags.elevation)?
					net.lugdunon.states.tiledGame.modes.terrain.SimpleButton.ICONS.D_ON:
					net.lugdunon.states.tiledGame.modes.terrain.SimpleButton.ICONS.D_OFF,
				doNotRegister:true,
				position     :{x:game.getCanvasWidthInPixels()-240, y:0}
			}
		);
		net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.brushSizeWidget=new net.lugdunon.states.tiledGame.modes.terrain.BrushSizeWidget().init({id:"tembsw",tileSize:game.tileSetDef.tileSize,field:this.fm});
	}
	else
	{
		net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.elevationUpButton    .delegate=this;
		net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.elevationDownButton  .delegate=this;
		net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.elevationToggleButton.delegate=this;
	}
	
	this.elevationUpButton    =net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.elevationUpButton;
	this.elevationDownButton  =net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.elevationDownButton;
	this.elevationToggleButton=net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.elevationToggleButton;
	this.brushSizeWidget      =net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.brushSizeWidget;
	
	this.brushSizeWidget.updateDelegates.push(this);

	//CONFIGURE BRUSH SIZE IMAGE
	this.brushSizeWidget.reconfigure();
	
	this.curElevLabel.setText("Active Elevation: "+this.elevation);
	
	return(this);
}; 

net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.prototype.screenResized=function()
{
	this.callSuper(net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode,"screenResized");

	this.elevationUpButton    .reposition(game.getCanvasWidthInPixels()- 64, 0);
	this.elevationDownButton  .reposition(game.getCanvasWidthInPixels()- 64,50);
	this.elevationToggleButton.reposition(game.getCanvasWidthInPixels()-240, 0);
};

net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.prototype.getActionButtonLabel=function()
{
	return("Raise / Lower Terrain");
};

net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.prototype.reactivated=function()
{
	this.callSuper(net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode,"reactivated");
	
	game.getInput().setCursor(null);
};

net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.prototype.activate=function()
{
	this.callSuper(net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode,"activate");
	
	this.brushSizeWidget      .registerWithInput();
	this.elevationUpButton    .registerWithInput();
	this.elevationDownButton  .registerWithInput();
	this.elevationToggleButton.registerWithInput();
	
	net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.elevationUpButton    .delegate=this;
	net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.elevationDownButton  .delegate=this;
	net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.elevationToggleButton.delegate=this;
	
	game.getInput().setCursor(null);

	this.teod=false;
	
//	if(game.uiSettings.getFlagValue(game.uiSettings.flags.elevation) === false)
//	{
//		this.teod=true;
//		game.uiSettings.toggleFlagValue("elevation");
//	}
};

net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.prototype.deactivate=function()
{
	this.callSuper(net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode,"deactivate");
	
	this.brushSizeWidget      .unregisterWithInput();
	this.elevationUpButton    .unregisterWithInput();
	this.elevationDownButton  .unregisterWithInput();
	this.elevationToggleButton.unregisterWithInput();
	
//	if(this.teod && (game.uiSettings.getFlagValue(game.uiSettings.flags.elevation) === true))
//	{
//		game.uiSettings.toggleFlagValue("elevation");
//	}
};

////

net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.prototype.updateBrush=function(size)
{
	this.boundingRect.w =size.w;
	this.boundingRect.h =size.h;
};

net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.prototype.update=function(delta)
{
	;
};

net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.prototype.draw=function(delta)
{
	game.getContext().pushState();
	
	if(this.cursorLoc.x > -1)
	{
		if(!Namespace.isMobile())
		{
			var modX=0;
			var modY=0;
			
			//DRAW RED SELECTOR ON SCREEN
			game.getContext().strokeStyle="#800";
			game.getContext().fillStyle  ="#800";
			game.getContext().lineWidth  =2;
			
			if(this.boundingRect.w == 3)
			{
				modX=-1;
			}
			
			if(this.boundingRect.h == 3)
			{
				modY=-1;
			}
			
			game.getContext().strokeRoundedRect(
				(Math.floor(this.cursorLoc.x/game.tileSetDef.tileSize)+modX)*game.tileSetDef.tileSize,
				(Math.floor(this.cursorLoc.y/game.tileSetDef.tileSize)+modY)*game.tileSetDef.tileSize,
				this.boundingRect.w*game.tileSetDef.tileSize,
				this.boundingRect.h*game.tileSetDef.tileSize,
				8
			);

			game.getContext().globalAlpha=0.5;
			game.getContext().fillRoundedRect(
				(Math.floor(this.cursorLoc.x/game.tileSetDef.tileSize)+modX)*game.tileSetDef.tileSize,
				(Math.floor(this.cursorLoc.y/game.tileSetDef.tileSize)+modY)*game.tileSetDef.tileSize,
				this.boundingRect.w*game.tileSetDef.tileSize,
				this.boundingRect.h*game.tileSetDef.tileSize,
				8
			);
		}
	}
	
	this.brushSizeWidget      .draw(delta);
	this.elevationUpButton    .draw(delta);
	this.elevationDownButton  .draw(delta);
	this.elevationToggleButton.draw(delta);

	game.getContext().popState();
	
	this.curElevLabel.draw(
		game.getContext(),
		delta,
		(game.getCanvasWidthInPixels()-this.bmWidth-72)-((this.curElevLabel.width()/2)-(this.bmWidth/2)),
		this.bmWidth+16
	);
};

////

net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.prototype.setTerrain=function(e)
{
	this.cursorLoc.x=e.x;
	this.cursorLoc.y=e.y;
	
	this.boundingRect.e=this.elevation;
	
	if(this.resetSend || ((this.boundingRect.x != Math.floor(this.cursorLoc.x/game.tileSetDef.tileSize)) || (this.boundingRect.y != Math.floor(this.cursorLoc.y/game.tileSetDef.tileSize))))
	{
		var modX=0;
		var modY=0;
		
		if(this.boundingRect.w == 3)
		{
			modX=-1;
		}
		
		if(this.boundingRect.h == 3)
		{
			modY=-1;
		}
		
		this.boundingRect.x=Math.floor(this.cursorLoc.x/game.tileSetDef.tileSize)+modX;
		this.boundingRect.y=Math.floor(this.cursorLoc.y/game.tileSetDef.tileSize)+modY;

		game.client.sendCommand(
			game.client.buildCommand(
				"CORE.COMMAND.EDIT.ELEVATION.SIMPLE",
				this.boundingRect
			)
		);
		
		this.resetSend=false;
	}
};

net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.prototype.buttonTriggered=function(button,e)
{
	if(button == this.elevationUpButton)
	{
		this.elevation++;
	}
	else if(button == this.elevationDownButton)
	{
		this.elevation--;
	}
	else if(button == this.elevationToggleButton)
	{
		this.elevationToggleButton.icon=
			game.uiSettings.toggleFlagValue("elevation")?
			net.lugdunon.states.tiledGame.modes.terrain.SimpleButton.ICONS.D_ON:
			net.lugdunon.states.tiledGame.modes.terrain.SimpleButton.ICONS.D_OFF;
	}
	
	if(this.elevation > net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.MAX_ELEVATION)
	{
		this.elevation=net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.MAX_ELEVATION;
	}
	else if(this.elevation < net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.MIN_ELEVATION)
	{
		this.elevation=net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.MIN_ELEVATION;
	}
	
	this.curElevLabel.setText("Active Elevation: "+this.elevation);
};

////

net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.prototype.handleInputBegin=function(e)
{
	this.setTerrain(e);
};

net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.prototype.handleInputEnd=function(e)
{
	this.resetSend=true;
};

net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.prototype.handleInputMove=function(e)
{
	this.cursorLoc.x=e.x;
	this.cursorLoc.y=e.y;
};

net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.prototype.handleInputDrag=function(e)
{
	this.setTerrain(e);
};

net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.prototype.handleInputIn=function(e)
{
	this.cursorLoc.x=e.x;
	this.cursorLoc.y=e.y;
	
	game.getInput().setCursor(net.lugdunon.input.Cursor.CURSORS.POINTER);
};

net.lugdunon.elevation.editor.simple.mode.TerrainElevationMode.prototype.handleInputOut=function(e)
{
	this.cursorLoc.x=-1;
	
	game.getInput().setCursor(net.lugdunon.input.Cursor.CURSORS.POINTER);
};